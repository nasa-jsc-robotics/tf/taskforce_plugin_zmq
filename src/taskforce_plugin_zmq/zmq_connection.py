import zmq
import logging

from taskforce_common import PubSub, ReqRep, Pair
from .zmq_port import ZMQPublisher, ZMQSubscriber, ZMQBidirectional, ZMQClient, ZMQServer

logger = logging.getLogger(__name__)

# http://api.zeromq.org/2-1:zmq-bind
zmq_transports = ['tcp', 'inproc', 'ipc', 'pgm', 'epgm']


class ZMQPubSub(PubSub):
    def __init__(self, topic='', transport='tcp', address='localhost', port='5555'):
        self.topic = topic
        self.transport = transport
        self.address = address
        self.port = port
        self.publisher = ZMQPublisher(self.topic, self.transport, self.port)

    def getPublisher(self):
        return self.publisher

    def getSubscriber(self):
        return ZMQSubscriber(self.topic, self.transport, self.address, self.port)


class ZMQReqRep(ReqRep):
    def __init__(self, transport='tcp', address='localhost', port='5555'):
        self.transport = transport
        self.address = address
        self.port = port
        self.server = ZMQServer(self.transport, self.port)

    def getServer(self):
        return self.server

    def getClient(self):
        return ZMQClient(self.transport, self.address, self.port)


class ZMQPair(Pair):
    def __init__(self):
        raise NotImplementedError


