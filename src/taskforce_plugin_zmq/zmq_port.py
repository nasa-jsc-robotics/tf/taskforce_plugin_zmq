import zmq
import cPickle
import logging
import time
from taskforce_common import Port, Publisher, Subscriber, Server, Client, Bidirectional

logger = logging.getLogger(__name__)

# http://api.zeromq.org/2-1:zmq-bind
zmq_transports = ['tcp', 'inproc', 'ipc', 'pgm', 'epgm']

ZMQ_POLL_TIMEOUT = 2500 # poll timeout in milliseconds
ZMQ_CONNECT_SLEEP = 0.100 #seconds

class ZMQConnectionException(Exception):
    pass

class ZMQPublisher(Publisher):
    def __init__(self, topic='', transport='tcp', port=5556):
        super(ZMQPublisher, self).__init__()
        if transport not in zmq_transports:
                raise ZMQConnectionException('Trying to create ZMQPublisher with invalid protocol:"{}". \
                                 Valid protocols are:{}'.format(transport,zmq_transports))
        self.logger = logger.getChild('ZMQPublisher')
        self.topic = topic
        self.transport = transport
        self.port = port

    def initialize(self):
        if not self.initialized: #Connection base-class variable
            # Check for valid transport argument
            self.context = zmq.Context()
            self.socket  = self.context.socket(zmq.PUB)
            self.initialized = True
            self.logger.debug('initialize')
            return True
        return False

    def connect(self):
        if not self.connected:
            if not self.initialized:
                init_ok = self.initialize()
                if not init_ok:
                    return False
            connectString = "{}://*:{}".format(self.transport,self.port)
            self.socket.bind(connectString)
            self.logger.debug('bind "{}" on topic "{}"'.format(connectString,self.topic))
            self.connected = True
            return True
        return False

    def disconnect(self):
        if self.connected:
            self.socket.close(0)
            self.connected = False
            return True
        return False

    def send(self, data):
        if not self.connected:
            connect_ok = self.connect()
            if not connect_ok:
                return False
        self.logger.debug('send: "{}"'.format(str(data)))
        self.socket.send_pyobj(data)


class ZMQSubscriber(Subscriber):
    def __init__(self, topic='', transport='tcp', address='localhost', port=5556):
        super(ZMQSubscriber, self).__init__()
        if transport not in zmq_transports:
                raise ZMQConnectionException('Trying to create ZMQSubscriber with invalid protocol:"{}". \
                                 Valid protocols are:{}'.format(transport,zmq_transports))
        self.logger = logger.getChild('ZMQSubscriber')
        self.topic = topic
        self.address = address
        self.transport = transport
        self.port = port

    def initialize(self):
        if not self.initialized: #Connection base-class variable
            # Check for valid transport argument
            self.context = zmq.Context()
            self.socket  = self.context.socket(zmq.SUB)
            self.socket.setsockopt_unicode(zmq.SUBSCRIBE, u'{}'.format(self.topic))
            self.poller = zmq.Poller()
            self.poller.register(self.socket, zmq.POLLIN)
            self.initialized = True
            self.logger.debug('initialize')
            return True
        return False

    def connect(self):
        if not self.connected:
            if not self.initialized:
                init_ok = self.initialize()
                if not init_ok:
                    return False
            connectString = "{}://{}:{}".format(self.transport,self.address,self.port)
            self.socket.connect(connectString)
            # after we connect, we need to give the connection a little bit of time to synch up with the
            # publisher.  If we don't, we may miss the first message
            time.sleep(ZMQ_CONNECT_SLEEP)
            self.logger.debug('connect "{}" on topic "{}"'.format(connectString,self.topic))
            self.connected = True
            return True
        return False

    def disconnect(self):
        if self.connected:
            self.socket.close(0)
            self.connected = False
            return True
        return False

    def receive(self):
        '''
        Poll for messages in a non-blocking manner

        This method is necessary in order to play nicely with starting and stopping the process
        '''
        if not self.connected:
            connect_ok = self.connect()
            if not connect_ok:
                return False
        try:
            # Ask the poller in there is new data
            sockets = dict(self.poller.poll(1000))
        except zmq.ZMQError, e:
            # The OS will occasionally abort the poll which throws an exception.
            # We are still waiting for a response so the poll is resumed.
            return None

        if (self.socket in sockets and sockets[self.socket] == zmq.POLLIN):
            return self.receiveData()
        return None

    def receiveData(self):
        data = self.socket.recv_pyobj()
        self.logger.debug('receive: "{}"'.format(str(data)))
        return data


class ZMQUnicodeSubscriber(ZMQSubscriber):
    def receiveData(self):
        message = self.socket.recv()
        self.logger.debug('message received: "{}"'.format(message))
        return message


class ZMQUnicodePublisher(ZMQPublisher):

    def send(self, data):
        if not self.connected:
            connect_ok = self.connect()
            if not connect_ok:
                return False
        pub_message = "{}:{}".format(self.topic,data)
        self.logger.debug('publish messge: "{}"'.format(pub_message))
        self.socket.send_unicode(u'{}'.format(pub_message))

class ZMQUnicodeSubscriber(ZMQSubscriber):
    pass

class ZMQServer(Server):

    def __init__(self, transport='tcp', port=5555):
        super(ZMQServer, self).__init__()
        if transport not in zmq_transports:
            raise Exception('Trying to create ZMQServer with invalid protocol:"{}". \
                             Valid protocols are:{}'.format(transport,zmq_transports))
        self.logger = logger.getChild('ZMQServer')
        self.transport = transport
        self.port      = port
        self.poll_timeout = ZMQ_POLL_TIMEOUT

    def initialize(self):
        if not self.initialized:
            self.context = zmq.Context()
            self.socket  = self.context.socket(zmq.REP)
            self.poller  = zmq.Poller()
            self.poller.register(self.socket,zmq.POLLIN)
            self.initialized = True
            return True
        return False

    def connect(self):
        if not self.connected:
            if not self.initialized:
                init_ok = self.initialize()
                if not init_ok:
                    return False
            connectString = "{}://*:{}".format(self.transport, self.port)
            self.socket.bind(connectString)
            self.logger.debug('bind "{}"'.format(connectString))
            self.connected = True
            return True
        return False

    def disconnect(self):
        if self.connected:
            self.socket.close(0)
            self.connected = False
            return True
        return False

    def waitForRequest(self):
        if not self.connected:
            connect_ok = self.connect()
            if not connect_ok:
                return False
        try:
            # Ask the poller in there is new data
            sockets = dict(self.poller.poll(self.poll_timeout))
        except zmq.ZMQError, e:
            # The OS will occasionally abort the poll which throws an exception.
            # We are still waiting for a response so the poll is resumed.
            return None

        if (self.socket in sockets and sockets[self.socket] == zmq.POLLIN):
            data = self.socket.recv_pyobj()
            self.logger.debug('request: "{}"'.format(str(data)))
            return data

        return None

    def reply(self, message):
        self.logger.debug('reply:{}'.format(str(message)))
        self.socket.send_pyobj(message)


class ZMQClient(Client):
    def __init__(self, transport='tcp', address='localhost', port=5555):
        super(ZMQClient, self).__init__()
        if transport not in zmq_transports:
            raise Exception('Trying to create ZMQClient with invalid protocol:"{}". \
                             Valid protocols are:{}'.format(transport,zmq_transports))
        self.logger    = logger.getChild('ZMQClient')
        self.address   = address
        self.transport = transport
        self.port      = port
        self.poll_timeout = ZMQ_POLL_TIMEOUT


    def initialize(self):
        if not self.initialized: #Connection base-class variable
            self.context = zmq.Context()
            self.socket  = self.context.socket(zmq.REQ)
            self.poller  = zmq.Poller()
            self.poller.register(self.socket, zmq.POLLIN)
            self.initialized = True
            self.logger.debug('initialize')
            return True
        return False

    def connect(self):
        if not self.connected:
            if not self.initialized:
                init_ok = self.initialize()
                if not init_ok:
                    return False
            connectString = "{}://{}:{}".format(self.transport,self.address,self.port)
            self.socket.connect(connectString)
            self.logger.debug('connect "{}"'.format(connectString))
            self.connected = True
            return True
        return False

    def disconnect(self):
        if self.connected:
            self.socket.close(0)
            self.connected = False
            return True
        return False

    def request(self, message):
        if not self.connected:
            connect_ok = self.connect()
            if not connect_ok:
                return False
        self.logger.debug('sending request:"{}"'.format(str(message)))
        self.socket.send_pyobj(message)
        try:
            # Ask the poller in there is new data
            sockets = dict(self.poller.poll(self.poll_timeout))
        except zmq.ZMQError, e:
            # The OS will occasionally abort the poll which throws an exception.
            # We are still waiting for a response so the poll is resumed.
            return None

        if (self.socket in sockets and sockets[self.socket] == zmq.POLLIN):
            data = self.socket.recv_pyobj()
            self.logger.debug('reply: "{}"'.format(str(data)))
            return data
        else:
            self.logger.warning('request timeout.  Disconnecting')
            self.disconnect()
            self.initialized = False

            return None
            #raise ZMQClientException('Client reply time-out request:"{}"'.format(str(data)))

        return None


class ZMQBidirectional(Bidirectional):
    def __init__(self):
        raise NotImplementedError

