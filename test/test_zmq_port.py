import unittest

from threading import Lock
from multiprocessing import Process
from threading import Thread
import time
import zmq

from taskforce_plugin_zmq import ZMQPublisher, ZMQSubscriber, ZMQConnectionException
from taskforce_plugin_zmq import ZMQServer, ZMQClient

def publish(pubType, data, init_args):
    pub = pubType(**init_args)
    pub.connect()
    # need to sleep to give subscriber time to connect
    time.sleep(0.2)
    pub.send(data)
    pub.disconnect()

def zmq_server(socket, reply):
    message = socket.recv_pyobj()
    socket.send_pyobj(message+reply)

class DataClass(object):
    a = 1
    b = 2
    s = 'this is a string'

    def __str__(self):
        return 'a={},b={},s={}'.format(self.a,self.b,self.s)

class TestZMQPublisher(unittest.TestCase):
    def setUp(self):
        self.context = zmq.Context()
        self.socket  = self.context.socket(zmq.SUB)
        topic     = ''
        transport = 'tcp'
        address   = 'localhost'
        port      = 5556
        connectString = "{}://{}:{}".format(transport, address, port)
        self.socket.connect(connectString)
        self.socket.setsockopt_unicode(zmq.SUBSCRIBE,u'{}'.format(topic))

        self.kwargs = {'topic':topic,'transport':transport,'port':port}

    def tearDown(self):
        self.socket.close(linger=0)

    def testConstructor(self):
        pub = ZMQPublisher(**self.kwargs)

        self.assertFalse(pub.initialized)
        self.assertFalse(pub.connected)

    def testWrongTransport(self):
        self.assertRaises(ZMQConnectionException,ZMQPublisher,**{'transport':'xyz'})

    def testInitialize(self):
        pub = ZMQPublisher(**self.kwargs)

        self.assertFalse(pub.initialized)
        self.assertFalse(pub.connected)
        self.assertTrue(pub.initialize())
        self.assertFalse(pub.initialize())
        self.assertTrue(pub.initialized)
        self.assertFalse(pub.connected)

    def testConnect(self):
        pub = ZMQPublisher(**self.kwargs)
        self.assertFalse(pub.initialized)
        self.assertFalse(pub.connected)

        self.assertTrue(pub.connect())
        self.assertFalse(pub.connect())
        self.assertTrue(pub.connected)
        self.assertTrue(pub.initialized)

    def testDisconnect(self):
        pub = ZMQPublisher(**self.kwargs)
        self.assertFalse(pub.initialized)
        self.assertFalse(pub.connected)

        self.assertTrue(pub.connect())
        self.assertFalse(pub.connect())
        self.assertTrue(pub.connected)
        self.assertTrue(pub.initialized)

        self.assertTrue(pub.disconnect())
        self.assertFalse(pub.disconnect())
        self.assertFalse(pub.connected)
        self.assertTrue(pub.initialized)

    def testSend_string(self):
        data = 'test message'
        pub_process = Process(target=publish,args=(ZMQPublisher,data,self.kwargs))
        pub_process.start()
        pub_process.join()
        self.assertEqual(self.socket.recv_pyobj(),data)

    def testSend_obj(self):
        data = DataClass()
        pub_process = Process(target=publish,args=(ZMQPublisher,data,self.kwargs))
        pub_process.start()
        pub_process.join()
        self.assertEqual(str(self.socket.recv_pyobj()),str(data))

class TestZMQSubscriber(unittest.TestCase):
    def setUp(self):
        topic     = ''
        transport = 'tcp'
        address   = 'localhost'
        port      = 5556
        self.kwargs = {'topic':topic,'transport':transport,'address':address,'port':port}

    def tearDown(self):
        pass
        #self.socket.close(linger=0)

    def testConstructor(self):
        sub = ZMQSubscriber(**self.kwargs)

        self.assertFalse(sub.initialized)
        self.assertFalse(sub.connected)

    def testWrongTransport(self):
        self.assertRaises(ZMQConnectionException,ZMQSubscriber,**{'transport':'xyz'})

    def testInitialize(self):
        sub = ZMQSubscriber(**self.kwargs)

        self.assertFalse(sub.initialized)
        self.assertFalse(sub.connected)
        self.assertTrue(sub.initialize())
        self.assertFalse(sub.initialize())
        self.assertTrue(sub.initialized)
        self.assertFalse(sub.connected)

    def testConnect(self):
        sub = ZMQSubscriber(**self.kwargs)
        self.assertFalse(sub.initialized)
        self.assertFalse(sub.connected)

        self.assertTrue(sub.connect())
        self.assertFalse(sub.connect())
        self.assertTrue(sub.connected)
        self.assertTrue(sub.initialized)

    def testDisconnect(self):
        sub = ZMQSubscriber(**self.kwargs)
        self.assertFalse(sub.initialized)
        self.assertFalse(sub.connected)

        self.assertTrue(sub.connect())
        self.assertFalse(sub.connect())
        self.assertTrue(sub.connected)
        self.assertTrue(sub.initialized)

        self.assertTrue(sub.disconnect())
        self.assertFalse(sub.disconnect())
        self.assertFalse(sub.connected)
        self.assertTrue(sub.initialized)

    def testRecv_string(self):
        data = 'test message'
        sub = ZMQSubscriber(**self.kwargs)
        self.assertTrue(sub.connect())

        pub_process = Process(target=publish,args=(ZMQPublisher,data,{'transport':'tcp','port':self.kwargs['port']}))
        pub_process.start()
        pub_process.join()
        self.assertEqual(sub.receive(),data)

    def testSend_obj(self):
        data = DataClass()
        sub = ZMQSubscriber(**self.kwargs)
        self.assertTrue(sub.connect())

        pub_process = Process(target=publish,args=(ZMQPublisher,data,{'transport':'tcp','port':self.kwargs['port']}))
        pub_process.start()
        pub_process.join()
        self.assertEqual(str(sub.receive()),str(data))

class TestZMQServer(unittest.TestCase):
    def setUp(self):
        self.context = zmq.Context()
        self.socket  = self.context.socket(zmq.REQ)
        topic     = ''
        transport = 'tcp'
        address   = 'localhost'
        port      = 5555
        connectString = "{}://{}:{}".format(transport, address, port)
        self.socket.connect(connectString)

    def tearDown(self):
        self.socket.close(linger=0)

    #@unittest.skip('reason')
    def test_constructor(self):
        server = ZMQServer()
        self.assertFalse(server.initialized)
        self.assertFalse(server.connected)
        self.assertFalse(hasattr(server,'socket'))

    #@unittest.skip('reason')
    def test_initialize(self):
        server = ZMQServer()

        self.assertTrue(server.initialize())
        self.assertTrue(server.initialized)
        self.assertFalse(server.connected)
        self.assertTrue(hasattr(server, 'socket'))
        self.assertTrue(hasattr(server, 'context'))
        self.assertTrue(hasattr(server, 'poller'))

        self.assertFalse(server.initialize())

    #@unittest.skip('reason')
    def test_connect(self):
        server = ZMQServer()

        self.assertTrue(server.connect())
        self.assertTrue(server.initialized)
        self.assertTrue(server.connected)

        self.assertFalse(server.connect())

    #@unittest.skip('reason')
    def test_disconnect(self):
        server = ZMQServer()

        self.assertFalse(server.disconnect())

        server.connect()
        self.assertTrue(server.disconnect())

        server2 = ZMQServer()

    #@unittest.skip('reason')
    def test_registerCallback(self):
        server = ZMQServer()
        self.assertRaises(Exception, server.registerCallback)

    #@unittest.skip('reason')
    def test_waitForRequest(self):
        server = ZMQServer()
        self.assertEqual(server.waitForRequest(), None)
        message = 'test message'
        self.socket.send_pyobj(message)
        self.assertEqual(server.waitForRequest(),message)

    #@unittest.skip('reason')
    def test_reply(self):
        server = ZMQServer()

        message = 'message'
        reply   = 'reply'

        self.socket.send_pyobj(message)
        self.assertEqual(server.waitForRequest(),message)
        server.reply(reply)
        self.assertEqual(self.socket.recv_pyobj(), reply)

class TestZMQClient(unittest.TestCase):
    def setUp(self):
        self.context = zmq.Context()
        self.socket  = self.context.socket(zmq.REP)
        topic     = ''
        transport = 'tcp'
        port      = 5555
        connectString = "{}://*:{}".format(transport, port)
        success = False
        while(not success):
            try:
                self.socket.bind(connectString)
                success = True
            except zmq.ZMQError:
                time.sleep(0.1)

    def tearDown(self):
        self.socket.close(linger=0)

    #@unittest.skip('reason')
    def test_constructor(self):
        client = ZMQClient()
        self.assertFalse(client.initialized)
        self.assertFalse(client.connected)
        self.assertFalse(hasattr(client,'socket'))

    #@unittest.skip('reason')
    def test_initialize(self):
        client = ZMQClient()

        self.assertTrue(client.initialize())
        self.assertTrue(client.initialized)
        self.assertFalse(client.connected)
        self.assertTrue(hasattr(client, 'socket'))
        self.assertTrue(hasattr(client, 'context'))
        self.assertTrue(hasattr(client, 'poller'))

        self.assertFalse(client.initialize())

    #@unittest.skip('reason')
    def test_connect(self):
        client = ZMQClient()

        self.assertTrue(client.connect())
        self.assertTrue(client.initialized)
        self.assertTrue(client.connected)

        self.assertFalse(client.connect())

    #@unittest.skip('reason')
    def test_disconnect(self):
        client = ZMQClient()

        self.assertFalse(client.disconnect())

        client.connect()
        self.assertTrue(client.disconnect())

        client2 = ZMQServer()

    #@unittest.skip('reason')
    def test_registerCallback(self):
        client = ZMQClient()
        self.assertRaises(Exception, client.registerCallback)

    #@unittest.skip('reason')
    def test_request(self):
        client = ZMQClient()

        request = 'request'
        reply   = 'reply'

        server_thread = Thread(target=zmq_server, args=(self.socket,reply))
        server_thread.start()

        self.assertEqual(client.request(request),request+reply)
