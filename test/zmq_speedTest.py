from multiprocessing import Process
import time
from  taskforce_plugin_zmq import ZMQSubscriber, ZMQPublisher
import logging

class Test(object):

    def __init__(self, name, topic, publisher, publishMethod, subscriber, subscribeMethod):
        self.name            = name
        self.topic           = topic
        self.publisher       = publisher
        self.publisherType   = publisher.__class__.__name__
        self.publishMethod   = publishMethod
        self.subscriber      = subscriber
        self.subscriberType  = subscriber.__class__.__name__
        self.subscribeMethod = subscribeMethod

        self.start_time = 0
        self.stop_time = 0
        self.totalTime = 0
        self.timePerMsg = 0

    def runTest(self, message, numMessages):
        self.message = message
        self.numMessages = numMessages

        subscribeProcess = Process(target=self._subscribeLoop)
        subscribeProcess.start()
        publish = self._initPublishLoop()
        self.start_time = time.time()
        count = 0
        while(count < self.numMessages):
            publish(self.message)
            count = count + 1

        subscribeProcess.join()
        self.stop_time = time.time()
        self.totalTime = self.stop_time - self.start_time
        self.timePerMsg = self.totalTime / self.numMessages
        print self.results

    def _initPublishLoop(self):
        publish = getattr(self.publisher, self.publishMethod)
        self.publisher.initialize()
        time.sleep(1)
        self.publisher.logger.setLevel(logging.INFO)
        return publish

    def _subscribeLoop(self):
        receive = getattr(self.subscriber, self.subscribeMethod)
        self.subscriber.initialize()
        self.subscriber.logger.setLevel(logging.INFO)
        count = 0
        while(count < self.numMessages):
            if (receive()):
                count = count + 1

    @property
    def results(self):
        return """
Publisher : {}
PubMethod : {}
Subscriber: {}
SubMethod : {}
Message   : {}
# messages: {}
total time: {}
avgMsgTime: {} ms""".format(self.publisherType,self.publishMethod,self.subscriberType,self.subscribeMethod, self.message,self.numMessages,self.totalTime, self.timePerMsg*1000)



if __name__ == "__main__":

    topic   = 'testTopic'
    message = 'TestMessage'
    numMessages = 1000000

    p1 = ZMQPublisher()
    s1 = ZMQSubscriber(topic)
    test1 = Test(name='Test1',
                 topic=topic,
                 publisher=p1,
                 publishMethod='send',
                 subscriber=s1,
                 subscribeMethod='receive')
    test1.runTest(message=message,numMessages=numMessages)

    s2 = ZMQSubscriber(topic)
    test2 = Test(name='Test2',
                 topic=topic,
                 publisher=p1,
                 publishMethod='send',
                 subscriber=s2,
                 subscribeMethod='receive')
    test2.runTest(message=message,numMessages=numMessages)

    p1.disconnect()
