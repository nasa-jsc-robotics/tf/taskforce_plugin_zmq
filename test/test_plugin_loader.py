import unittest
import os
from taskforce_common.utils import importPlugins

from taskforce_plugin_zmq import ZMQPublisher, ZMQServer

TEST_FOLDER = os.path.dirname(os.path.abspath(__file__))
PLUGIN_TEST_FILE = os.path.join(TEST_FOLDER, "default_task_engine_plugins.json")

class TestLoadDefaultPlugins(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_importPlugins(self):
        pluginDict = importPlugins(PLUGIN_TEST_FILE)

        servers = pluginDict['Engine']['Ports']['Servers']
        self.assertEqual(1, len(servers))
        self.assertEqual(ZMQServer, type(servers[0]))

        publishers = pluginDict['Engine']['Ports']['Publishers']
        self.assertEqual(1, len(publishers))
        self.assertEqual(ZMQPublisher, type(publishers[0]))

if __name__ == "__main__":
    unittest.main()