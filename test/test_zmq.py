from multiprocessing import Process

from taskforce_plugin_zmq import ZMQPublisher, ZMQSubscriber

import time

def publisher(init_args):
    pub = ZMQPublisher(**init_args)
    pub.initialize()
    pub.connect()
    time.sleep(0.2)
    pub.send('test message')

def subscriber(init_args):
    sub = ZMQSubscriber(**init_args)
    sub.initialize()
    sub.connect()
    received = None
    while received == None:
        received = sub.receive()

if __name__ == "__main__":
    p = Process(target=publisher, args=({},))
    s = Process(target=subscriber, args=({},))

    s.start()
    p.start()
    s.join()
    p.join()

    # sub = ZMQSubscriber()
    # sub.initialize()
    # sub.connect()
    # p.join()
    # sub.receive()