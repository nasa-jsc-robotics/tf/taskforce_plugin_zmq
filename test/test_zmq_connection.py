import unittest
import time

from taskforce_plugin_zmq import ZMQPubSub, ZMQPair, ZMQReqRep

def publish(pubType, data, init_args):
    pub = pubType(**init_args)
    pub.connect()
    # need to sleep to give subscriber time to connect
    time.sleep(0.2)
    pub.send(data)
    pub.disconnect()


class DataClass(object):
    a = 1
    b = 2
    s = 'this is a string'

    def __str__(self):
        return 'a={},b={},s={}'.format(self.a,self.b,self.s)

class TestZMQPubSub(unittest.TestCase):
    def setUp(self):

        topic     = ''
        transport = 'tcp'
        address   = 'localhost'
        port      = 5555
        self.kwargs = {'topic':topic, 'address':address, 'transport':transport,'port':port}

    def tearDown(self):
        pass

    def testGetPubSub(self):
        connection = ZMQPubSub(**self.kwargs)
        pub  = connection.getPublisher()
        data = DataClass()

        sub1 = connection.getSubscriber()
        sub2 = connection.getSubscriber()
        sub3 = connection.getSubscriber()

        sub1.connect()
        sub2.connect()
        sub3.connect()

        pub.connect()
        time.sleep(0.2)
        pub.send(data)

        self.assertEqual(str(sub1.receive()),str(data))
        self.assertEqual(str(sub2.receive()),str(data))
        self.assertEqual(str(sub3.receive()),str(data))